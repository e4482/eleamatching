<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main epikmatching configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_epikmatching
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_epikmatching\local\models\EpikmatchingActivity;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/epikmatching/locallib.php');

/**
 * Module instance settings form
 */
class mod_epikmatching_mod_form extends moodleform_mod {

    /**
     * Defines forms elements
     */
    public function definition() {

        global $CFG;
        $mform = $this->_form;

        //-------------------------------------------------------------------------------
        // Adding the "general" fieldset, where all the common settings are showed
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field
        $mform->addElement('text', 'name', "Titre : ", array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'epikmatchingname', 'epikmatching');

        // Adding the standard "intro" and "introformat" fields
        $this->standard_intro_elements();

        $themes = epikmatching_extract_themes();
        $mform->addElement('select', 'theme', "thème", $themes);
        //-------------------------------------------------------------------------------

        $mform->addElement('text', "grade", "nombre de points", array('size'=>'5'));
        $mform->setType("grade", PARAM_TEXT);
        $mform->setDefault('grade', 10);
        $mform->addHelpButton('grade', 'grade', 'epikmatching');

        $style = 'min-width:0px;';
        $style_container = 'display:table;width:100%;padding:10px;border-radius:5px;background-color:#aaaaaa;';

        // defined in database
        $MAX_FIELDS = 9;
        for ($i = 1; $i <= $MAX_FIELDS; $i++) {
            $mform->addElement('html', '<h4>paire ' . $i . '</h4>');
            $mform->addElement('html', '<div style="' . $style_container . '">');
            $mform->addElement('html', '<div style="display:table-cell;width:50%;">');
            $this->addTextField($mform, "pair1_$i", null, $style);
            $mform->addElement('html', '</div>');
            $mform->addElement('html', '<div style="display:table-cell;width:50%;">');
            $this->addTextField($mform, "pair2_$i", null, $style);
            $mform->addElement('html', '</div>');
            $mform->addElement('html', '</div>');
        }

        //-------------------------------------------------------------------------------
        // add standard elements, common to all modules
        $this->standard_coursemodule_elements();
        //-------------------------------------------------------------------------------
        // add standard buttons, common to all modules
        $this->add_action_buttons();
    }
    /**
     * Defines forms elements
     */
    public function addTextField($mform, $id, $label, $style='') {
        $mform->addElement('editor', $id . "_editor", $label, array('size'=>'64', 'style'=>$style), array('maxfiles' => EDITOR_UNLIMITED_FILES));
        $mform->setType($id, PARAM_RAW);
    }


    // called before form display and after form validation

    function data_preprocessing(&$default_values) {

        // update form
        if ($this->current->instance) {
            $epikmatching = new EpikmatchingActivity((int)$this->current->instance);
            $pairs = $epikmatching->getPairs();
            for ($i = 0 ; $i < count($pairs) ; $i++) {
                $pair = $pairs[$i];
                foreach (['left', 'right'] as $j => $side) {
                    $elementname = "pair" . ($j+1) . "_" . ($i+1);
                    $draftitemid = file_get_submitted_draft_itemid($elementname);
                    $editor = $pair->$side;
                    $text = $editor->prepare_file_draft_area($draftitemid);
                    $default_values["${elementname}_editor"] = [
                        "text" => $text,
                        "format" => $editor->format,
                        "itemid" => $draftitemid];

                }
            }
        }
    }

    // called just before form is displayed
    function definition_after_data(){
        global $CFG, $DB;
        // mandatory for completion !
        parent::definition_after_data();
    }
}
