<?php

namespace mod_epikmatching\local\models;

global $CFG;
require_once($CFG->dirroot . '/mod/epikmatching/locallib.php');
require_once($CFG->dirroot . '/lib/questionlib.php');
require_once($CFG->dirroot . '/mod/epikmatching/lib.php');

use context_module;
use question_edit_contexts;
use stdClass;
use dml_exception;
use Exception;
use vikimodule\Editor;
use vikimodule\Activity;
use vikimodule\Question;
use vikimodule\multichoice\MultichoiceAnswer;
use vikimodule\multichoice\MultichoiceQuestion;

/**
 * Class EpikmatchingQuestion
 *
 * Represent an Epikmatching full question (ie activity).
 * Fields mirrors DB fields.
 *
 * @package mod_epikmatching\local\models
 */
class EpikmatchingActivity extends Activity
{
    public static $fileoptions = [
        'maxfiles' => 0
    ];

    private $qtypeconversion;
    private $pairs;

    protected function table(): string
    {
        return MOD_EPIKMATCHING_MODNAME;
    }

    protected function component(): string
    {
        return MOD_EPIKMATCHING_COMPONENT;
    }

    protected function moduleid(): int
    {
        global $DB;
        return $DB->get_record(
            'modules', ['name' => 'epikmatching'], 'id', MUST_EXIST)->id;
    }

    protected function create_from_form_specific(stdClass $form): void {
        $this->grade = $form->grade;

        $this->pairs = [];
        for ($i = 1; $i <= MOD_EPICKMATCHING_MAX_NUMBER_OF_PAIRS; $i++) {
            $editors = [];
            $isvalid = true;
            foreach ([1 => 'left', 2 => 'right'] as $j => $side) {
                $elementname = "pair${j}_${i}_editor";
                $editorinform = $form->$elementname;
                $editor = new Editor($editorinform['text'], $editorinform['format'], $editorinform['itemid']);
                if ($editor->is_empty()) {
                    $isvalid = false;
                }
                $editors[$side] = $editor;
            }

            if ($isvalid) {
                $this->pairs[] = new EpikmatchingPair($editors['left'], $editors['right']);
            }
        }

        $this->coursemodule = $form->coursemodule;

        // Will be built at saving time
        $this->questionids = '';
    }

    protected function load_from_db_specific(stdClass $indb): void {
        global $DB;

        // Build pairs
        $questionids = explode(Question::QUESTION_DELIMITER, $this->questionids);
        if (empty($questionids[count($questionids)-1])) {
            array_pop($questionids);
        }

        if (empty($questionids)) {
            return;
        }
        $this->qtypeconversion = $DB->get_record('question', ['id' => $questionids[0]], 'qtype', MUST_EXIST)->qtype;
        switch ($this->qtypeconversion) {
            case 'multichoice':
                $this->load_multichoice_questions($questionids);
                break;
            case 'match':
                if (count($questionids) !== 1) {
                    throw new Exception('Inconsistency between number of questions and qtype (match)');
                }
                $this->load_match_question($questionids[0]);
                break;
            default:
                throw new Exception('Not manage case: question is not a multichoice nor a match question');
        }
    }

    private function load_multichoice_questions(array $questionids) {
        global $DB;

        $this->pairs = [];
        $this->questions = [];
        foreach ($questionids as $questionid) {
            $question = new MultichoiceQuestion((int)$questionid);
            $answer = $question->get_correct_answers()[0];
            $this->pairs[] = new EpikmatchingPair(
                $question->get_question(), $answer->get_answer()
            );
            $this->questions[] = $question;
        }
    }

    private function save_multichoice_questions() {
        global $DB;

        $this->qtypeconversion = 'multichoice';

        // Category
        $context = context_module::instance($this->coursemodule);
        $contexts = new question_edit_contexts($context);
        $categoryid = question_make_default_categories(array($contexts->lowest()))->id;


        // Preparing answers
        $answers = [];
        foreach ($this->pairs as $pair) {
            $answers[] = new MultichoiceAnswer(0, $pair->right);
        }

        // Retrieving old questions
        $dbrecord = $DB->get_record(
            'epikmatching',
            ['id' => $this->id],
            'questionids');
        if (!empty($dbrecord)) {
            $oldquestionids = $dbrecord->questionids;

            if (is_int($oldquestionids)) {
                $this->delete_match_question();
                $oldquestionids = [];
            } else {
                $oldquestionids = explode(Question::QUESTION_DELIMITER, $oldquestionids);
                // Removing last empty question id
                if (empty($oldquestionids[count($oldquestionids) - 1])) {
                    array_pop($oldquestionids);
                }
            }
        } else {
            $oldquestionids = [];
        }

        // Creating or updating questions
        $newquestionids = [];
        $this->questions = [];
        for ($index = 0 ; $index < count($this->pairs) ; $index++) {
            if ($index < count($oldquestionids)) {
                $questionid = $oldquestionids[$index];
            } else {
                $questionid = null;
            }

            // Copy answers for this question, setting iscorrect to true for the one matching the question
            $theseanswers = [];
            foreach ($answers as $answer) {
                $theseanswers[] = clone $answer;
            }
            $theseanswers[$index]->set_iscorrect(true);

            // Update already existing answers retrieving id. If questionid is empty, no answers in DB.
            $answersdb = array_values($DB->get_records('question_answers', ['question' => $questionid]));
            for ($iA = 0 ; $iA < count($answersdb) ; $iA++) {
                if ($iA < count($theseanswers)) {
                    $theseanswers[$iA]->set_id($answersdb[$iA]->id);
                } else {
                    try {
                        $DB->delete_records('question_answers', ['id' => $answersdb[$iA]->id]);
                    } catch (dml_exception $e) {
                        // TODO
                    }
                }
            }

            $pair = $this->pairs[$index];

            $question = new MultichoiceQuestion(
                $questionid, 'match question', $pair->left, $theseanswers, $categoryid);
            $this->questions[] = $question;
            $newquestionids[] = $question->save($this->coursemodule, MOD_EPIKMATCHING_MODNAME);
        }

        // Deleting old questions
        for ($index = count($this->pairs) ; $index < count($oldquestionids) ; $index++) {
            question_delete_question($oldquestionids[$index]);
        }

        // Building DB output (ultimate purpose of this function)
        $this->questionids = implode(Question::QUESTION_DELIMITER, $newquestionids);
    }

    private function load_match_question(int $questionid) {
        // TODO
    }

    private function delete_match_question() {
        // TODO
    }

    private function save_match_question() {
        // TODO
        $this->qtypeconversion = 'match';
        return $this->save_multichoice_questions();
    }

    protected function save_questions(): void {
        // Check if right size is compatible with match question, else choose multichoice
        $matchcompatible = true;
        foreach ($this->pairs as $pair) {
            $matchcompatible &= $pair->is_compatible_with_match_question();
        }

        if ($matchcompatible) {
            $this->save_match_question();
        } else {
            $this->save_multichoice_questions();
        }

    }

    public function save() {
        global $DB;

        // To be deleted when fieldd are dropped
        for ($i = 1; $i <= 9; $i++) {
            for ($j = 1; $j <= 2; $j++ ) {
                $fieldname = "pair{$j}_{$i}";
                $this->$fieldname = '';
            }
        }

        // Create record if not already done
        if ($this->id == 0) {
            $this->id = $DB->insert_record('epikmatching', $this, true);
        }

        $this->save_questions();

        $DB->update_record('epikmatching', $this);

        // useful for grade
        epikmatching_grade_item_update($this);
    }

    /**
     * @return array of EpikmatchingPair
     */
    public function getPairs()
    {
        return $this->pairs;
    }

    public function get_questionids()
    {

        switch ($this->qtypeconversion) {
            case 'multichoice':
                return explode(Question::QUESTION_DELIMITER, $this->questionids);
            case 'match':
                return array($this->questionids);
            default:
                throw new Exception('Not manage case: question is not a multichoice nor a match question');

        }
    }

    public static function from_old_version($oldmodule, $moduleid = null): EpikmatchingActivity
    {
        $form = clone $oldmodule;
        // Upgrade of platform: keep id, restoring from an old version: creating new epikmatching
        if (!empty($moduleid)) {
            unset($form->id);
        } else  {
            $form->instance = $oldmodule->id;
        }
        for ($i = 1 ; $i <= 9 ; $i++) {
            for ($j = 1 ; $j <= 2 ; $j++) {
                $dbfield = "pair${j}_$i";
                $formfield = "${dbfield}_editor";
                $form->$formfield['text'] = $oldmodule->$dbfield;
                $form->$formfield['format'] = FORMAT_HTML;
                $form->$formfield['itemid'] = 0; // no file update, done while updating files
            }
        }
        $form->coursemodule = $moduleid;
        $activity = new EpikmatchingActivity($form);
        $activity->update_course_module();

        return $activity;
    }

    protected function old_file_areas_to_delete(): array
    {
        $fileareas = [];
        for ($i = 1 ; $i <= 9 ; $i++) {
            for ($j = 1; $j < 2; $j++) {
                $fileareas[] = "pair${j}_$i";
            }
        }
        return $fileareas;
    }
}