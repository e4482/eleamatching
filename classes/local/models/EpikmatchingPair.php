<?php


namespace mod_epikmatching\local\models;

use vikimodule\Editor;

class EpikmatchingPair
{
    /**
     * @var Editor Left side of the pair
     */
    public $left;
    /**
     * @var Editor Right side of the pair
     */
    public $right;

    public function __construct(Editor $left, Editor $right)
    {
        $this->left = $left;
        $this->right = $right;
    }

    /**
     * @return bool true if the right side of the pair doesn't contain media as it is
     * the condition to be compatible with match question type.
     */
    public function is_compatible_with_match_question(): bool {
        return
            strpos($this->right->text, '@@PLUGIN_FILE@@') === false
            && strpos($this->right->text, 'draftfile.php') === false;
    }


}