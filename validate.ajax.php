<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Ajax script to update the contents of the question bank dialogue.
 *
 * @package    mod_epikmatching
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_epikmatching\local\models\EpikmatchingActivity;

define('AJAX_SCRIPT', true);
global $DB, $USER;

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/epikmatching/locallib.php');
require_once($CFG->dirroot . '/mod/epikmatching/lib.php');

$epikmatchingid = required_param('mid', PARAM_INT);             // The forum to subscribe or unsubscribe.
$attempts = required_param('attempts', PARAM_INT);
//$sesskey        = required_param('sesskey', PARAM_INT);
$epikmatching   = new EpikmatchingActivity($epikmatchingid);
$course         = $DB->get_record('course', array('id' => $epikmatching->course), '*', MUST_EXIST);
$cm             = get_coursemodule_from_instance('epikmatching', $epikmatching->id, $course->id, false, MUST_EXIST);
$context        = context_module::instance($cm->id);

require_login($course, false, $cm);

$return = new stdClass();

if (is_guest($context, $USER)) {
    // is_guest should be used here as this also checks whether the user is a guest in the current course.
    // Guests and visitors cannot subscribe - only enrolled users.
    throw new moodle_exception('noguestsubscribe', 'mod_epikmatching');
}

error_log("manage completion");
// Manage completion
$completion = new completion_info($course);
$completion->set_module_viewed($cm);
$completion->update_state($cm,COMPLETION_COMPLETE);

error_log("manage gradeBook");
// Manage gradeBook
$epikmatchingSuccess = $DB->get_record('epikmatching_success', array('userid' => $USER->id, 'epikmatching' => $epikmatching->id));
if (!$epikmatchingSuccess) {
    $epikmatchingSuccess = new stdClass();
    $epikmatchingSuccess->epikmatching = $epikmatching->id;
    $epikmatchingSuccess->userid = $USER->id;
    $epikmatchingSuccess->timemodified = time();
    $epikmatchingSuccess->attempts = $attempts;
    $DB->insert_record("epikmatching_success", $epikmatchingSuccess);
}

epikmatching_update_grades($epikmatching, $USER->id, true);

$contents = "Waow ! success my friend !";
echo json_encode(array(
    'status'   => 'OK',
    'contents' => $contents,
));
