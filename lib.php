<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module epikmatching
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the epikmatching specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_epikmatching
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('autoloader.php');
use mod_epikmatching\local\models\EpikmatchingActivity;

defined('MOODLE_INTERNAL') || die();



define('MOD_EPIKMATCHING_FRANKY','mod_epikmatching');
define('MOD_EPIKMATCHING_COMPONENT','mod_epikmatching');
define('MOD_EPIKMATCHING_TABLE','epikmatching');
define('MOD_EPIKMATCHING_USERTABLE','epikmatching');
define('MOD_EPIKMATCHING_MODNAME','epikmatching');
define('MOD_EPIKMATCHING_URL','/mod/epikmatching');
define('MOD_EPIKMATCHING_CLASS','mod_epikmatching');
define('MOD_EPIKMATCHING_GRADEHIGHEST', 0);
define('MOD_EPIKMATCHING_GRADELOWEST', 1);
define('MOD_EPIKMATCHING_GRADELATEST', 2);
define('MOD_EPIKMATCHING_GRADEAVERAGE', 3);
define('MOD_EPIKMATCHING_GRADENONE', 4);
define('MOD_EPICKMATCHING_MAX_NUMBER_OF_PAIRS', 9);

////////////////////////////////////////////////////////////////////////////////
// Moodle core API                                                            //
////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function epikmatching_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:                 return true;
        case FEATURE_SHOW_DESCRIPTION:          return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:   return true;
        case FEATURE_GRADE_HAS_GRADE:           return true;
        case FEATURE_GRADE_OUTCOMES:            return true;
        case FEATURE_BACKUP_MOODLE2:            return true;
        default:                                return null;
    }
}

/**
 * Implementation of the function for printing the form elements that control
 * whether the course reset functionality affects the epikmatching.
 *
 * @param $mform form passed by reference
 */
function epikmatching_reset_course_form_definition(&$mform) {
    $mform->addElement('header', MOD_EPIKMATCHING_MODNAME . 'header', get_string('modulenameplural', MOD_EPIKMATCHING_COMPONENT));
    $mform->addElement('advcheckbox', 'reset_' . MOD_EPIKMATCHING_MODNAME , get_string('deletealluserdata',MOD_EPIKMATCHING_COMPONENT));
}
/**
 * Course reset form defaults.
 * @param object $course
 * @return array
 */
function epikmatching_reset_course_form_defaults($course) {
    return array('reset_' . MOD_EPIKMATCHING_MODNAME =>1);
}
/**
 * Removes all grades from gradebook
 *
 * @global stdClass
 * @global object
 * @param int $courseid
 * @param string optional type
 */
function epikmatching_reset_gradebook($courseid, $type='') {
    global $CFG, $DB;
    $sql = "SELECT l.*, cm.idnumber as cmidnumber, l.course as courseid
              FROM {" . MOD_EPIKMATCHING_TABLE . "} l, {course_modules} cm, {modules} m
             WHERE m.name='" . MOD_EPIKMATCHING_MODNAME . "' AND m.id=cm.module AND cm.instance=l.id AND l.course=:course";
    $params = array ("course" => $courseid);
    if ($moduleinstances = $DB->get_records_sql($sql,$params)) {
        foreach ($moduleinstances as $moduleinstance) {
            epikmatching_grade_item_update($moduleinstance, 'reset');
        }
    }
}
/**
 * Actual implementation of the reset course functionality, delete all the
 * epikmatching attempts for course $data->courseid.
 *
 * @global stdClass
 * @global object
 * @param object $data the data submitted from the reset course.
 * @return array status array
 */
function epikmatching_reset_userdata($data) {
    global $CFG, $DB;
    $componentstr = get_string('modulenameplural', MOD_EPIKMATCHING_COMPONENT);
    $status = array();
    $params = array ("course" => $data->courseid);
    //$DB->delete_records(MOD_EPIKMATCHING_USERTABLE, $params);
    // remove all grades from gradebook
    if (empty($data->reset_gradebook_grades)) {
        epikmatching_reset_gradebook($data->courseid);
    }
    $status[] = array('component'=>$componentstr, 'item'=>get_string('deletealluserdata', MOD_EPIKMATCHING_COMPONENT), 'error'=>false);
    /// updating dates - shift may be negative too
    if ($data->timeshift) {
        shift_course_mod_dates(MOD_EPIKMATCHING_MODNAME, array('available', 'deadline'), $data->timeshift, $data->courseid);
        $status[] = array('component'=>$componentstr, 'item'=>get_string('datechanged'), 'error'=>false);
    }
    return $status;
}
/**
 * Saves a new instance of the epikmatching into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $epikmatching An object from the form in mod_form.php
 * @param mod_epikmatching_mod_form $mform
 * @return int The id of the newly inserted epikmatching record
 */
function epikmatching_add_instance(stdClass $epikmatching, mod_epikmatching_mod_form $mform = null) {

    $epikmatching->timecreated = time();
    $epikmatching->timemodified = time();

    $epikmatchingquestion = new EpikmatchingActivity($epikmatching);
    $epikmatchingquestion->save();

    return $epikmatchingquestion->id;
}

/**
 * Updates an instance of the epikmatching in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $epikmatching An object from the form in mod_form.php
 * @param mod_epikmatching_mod_form $mform
 * @return boolean Success/Fail
 */
function epikmatching_update_instance(stdClass $epikmatching, mod_epikmatching_mod_form $mform = null) {

    $epikmatchingquestion = new EpikmatchingActivity($epikmatching);
    $epikmatchingquestion->save();

    epikmatching_grade_item_update($epikmatchingquestion);
    return $epikmatchingquestion->id;
}

/**
 * Removes an instance of the epikmatching from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function epikmatching_delete_instance($id) {
    global $DB;

    if (! $epikmatching = $DB->get_record('epikmatching', array('id' => $id))) {
        return false;
    }

    # Delete any dependent records here #
    $DB->delete_records('epikmatching', array('id' => $epikmatching->id));

    return true;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return stdClass|null
 */
function epikmatching_user_outline($course, $user, $mod, $epikmatching) {

    $return = new stdClass();
    $return->time = 0;
    $return->info = '';
    return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * @param stdClass $course the current course record
 * @param stdClass $user the record of the user we are generating report for
 * @param cm_info $mod course module info
 * @param stdClass $epikmatching the module instance record
 * @return void, is supposed to echp directly
 */
function epikmatching_user_complete($course, $user, $mod, $epikmatching) {
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in epikmatching activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @return boolean
 */
function epikmatching_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;  //  True if anything was printed, otherwise false
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link epikmatching_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function epikmatching_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@see epikmatching_get_recent_mod_activity()}

 * @return void
 */
function epikmatching_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * @return boolean
 * @todo Finish documenting this function
 **/
function epikmatching_cron () {
    return true;
}

/**
 * Returns all other caps used in the module
 *
 * @example return array('moodle/site:accessallgroups');
 * @return array
 */
function epikmatching_get_extra_capabilities() {
    return array();
}

////////////////////////////////////////////////////////////////////////////////
// Gradebook API                                                              //
////////////////////////////////////////////////////////////////////////////////

/**
 * Is a given scale used by the instance of epikmatching?
 *
 * This function returns if a scale is being used by one epikmatching
 * if it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $epikmatchingid ID of an instance of this module
 * @return bool true if the scale is used by the given epikmatching instance
 */
function epikmatching_scale_used($epikmatchingid, $scaleid) {
    global $DB;

    /** @example */
    if ($scaleid and $DB->record_exists('epikmatching', array('id' => $epikmatchingid, 'grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Checks if scale is being used by any instance of epikmatching.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param $scaleid int
 * @return boolean true if the scale is used by any epikmatching instance
 */
function epikmatching_scale_used_anywhere($scaleid) {
    global $DB;

    /** @example */
    if ($scaleid and $DB->record_exists('epikmatching', array('grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Creates or updates grade item for the give epikmatching instance
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass|EpikmatchingActivity $epikmatching instance object with extra cmidnumber and modname property
 * @param mixed optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return void
 */
function epikmatching_grade_item_update($epikmatching, $grades=null) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    $item = array();
    $item['itemname'] = clean_param($epikmatching->name, PARAM_NOTAGS);
    $item['gradetype'] = GRADE_TYPE_VALUE;
    $item['grademax']  = $epikmatching->grade;
    $item['grademin']  = 0;

    if ($grades  === 'reset') {
        $item['reset'] = true;
        $grades = NULL;
    }
    // weird fix (from mod/quiz)
    if (!isset($epikmatching->id)) $epikmatching->id = $epikmatching->id;

    return grade_update('mod/epikmatching', $epikmatching->course, 'mod', 'epikmatching', $epikmatching->id, 0, $grades, $item);

}

/**
 * Update epikmatching grades in the gradebook
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass|EpikmatchingActivity $epikmatching instance object with extra cmidnumber and modname property
 * @param int $userid update grade of specific user only, 0 means all participants
 * @return void
 */
function epikmatching_update_grades($epikmatching, $userid = 0, $success = false) {
    global $CFG, $DB;
    require_once($CFG->libdir.'/gradelib.php');

    if ($userid) {
        $grade = new stdClass();
        $grade->userid   = $userid;
        if ($success) {
            $grade->rawgrade = $epikmatching->grade;
        } else {
            $grade->rawgrade = null;
        }
        $grade->timemodified = time();
        epikmatching_grade_item_update($epikmatching, $grade);

    } else {
        epikmatching_grade_item_update($epikmatching);
    }
}



////////////////////////////////////////////////////////////////////////////////
// File API                                                                   //
////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function epikmatching_get_file_areas($course, $cm, $context) {
    return array('somearea' => 'somearea');
}

/**
 * File browsing support for epikmatching file areas
 *
 * @package mod_epikmatching
 * @category files
 *
 * @param file_browser $browser
 * @param array $areas
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param int $itemid
 * @param string $filepath
 * @param string $filename
 * @return file_info instance or null if not found
 */
function epikmatching_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    return null;
}

/**
 * Serves the files from the epikmatching file areas
 *
 * @package mod_epikmatching
 * @category files
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the epikmatching's context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 */
function epikmatching_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options=array()) {
    global $DB, $CFG;

    require_login($course, true, $cm);
    $entryid = (int)array_shift($args);
    $relativepath = implode('/', $args);
    if ($filearea == 'questiontext') {
        $component = 'question';
        $sql = 'SELECT cont.id FROM {context} AS cont
              INNER JOIN {question_categories} AS qc
              ON cont.id = qc.contextid
              INNER JOIN {question} AS q
              ON q.category = qc.id
              WHERE q.id = :qid';
        $filecontextid = $DB->get_record_sql($sql, ['qid' => $entryid], MUST_EXIST)->id;
    } else if ($filearea == 'answer') {
        $component = 'question';
        $sql = 'SELECT cont.id FROM {context} AS cont
              INNER JOIN {question_categories} AS qc
              ON cont.id = qc.contextid
              INNER JOIN {question} AS q
              ON q.category = qc.id
              INNER JOIN {question_answers} as ans
              ON ans.question = q.id
              WHERE ans.id = :ansid';
        $filecontextid = $DB->get_record_sql($sql, ['ansid' => $entryid], MUST_EXIST)->id;
    } else {
        // Not used for now as intro files are not directed there and there is no other text editor than intro and questions/answers
        $component = MOD_EPIKMATCHING_COMPONENT;
        $filecontextid = context_module::instance($cm->id, MUST_EXIST)->id;
    }
    $fullpath = "/$filecontextid/$component/$filearea/$entryid/$relativepath";
    $fs = get_file_storage();
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // finally send the file
    send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!

}

////////////////////////////////////////////////////////////////////////////////
// Navigation API                                                             //
////////////////////////////////////////////////////////////////////////////////

/**
 * Extends the global navigation tree by adding epikmatching nodes if there is a relevant content
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $navref An object representing the navigation tree node of the epikmatching module instance
 * @param stdClass $course
 * @param stdClass $module
 * @param cm_info $cm
 */
function epikmatching_extend_navigation(navigation_node $navref, stdclass $course, stdclass $module, cm_info $cm) {
}

/**
 * Extends the settings navigation with the epikmatching settings
 * - button to convert this module into a quiz module
 *
 * This function is called when the context for the page is a epikmatching module. This is not called by AJAX
 * so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav {@link settings_navigation}
 * @param navigation_node $epikmatchingnode {@link navigation_node}
 */
function epikmatching_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $epikmatchingnode=null) {
    global $PAGE;
    $cmcontext = $PAGE->context;

    if ($cmcontext instanceof context_module) {
        $coursecontext = $cmcontext->get_course_context();
    }

    if ($coursecontext && has_capability('mod/quiz:addinstance', $coursecontext)) {
        global $DB;
        $course = $DB->get_record('course', ['id' => $coursecontext->instanceid], 'format', MUST_EXIST);
        $convertnode = navigation_node::create(
            get_string('converttoquiz', 'epikmatching'),
            new moodle_url('/mod/epikmatching/convert.php', ['cmid' => $cmcontext->instanceid, 'courseid' => $coursecontext->instanceid])
        );

        $beforekey = null;

        $epikmatchingnode->add_node($convertnode, $beforekey);
    }
}

/**
 * Obtains the automatic completion state for this epikmatching based on any conditions
 * in epikmatching settings.
 *
 * @global object
 * @global object
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function epikmatching_get_completion_state($course,$cm,$userid,$type) {
    global $CFG,$DB;

    // Get forum details
    if (!($epikmatching=$DB->get_record('epikmatching',array('id'=>$cm->instance)))) {
        throw new Exception("Can't find epikmatching {$cm->instance}");
    }

    $result=$type; // Default return value

    $postcountparams=array('userid'=>$userid,'epikmatchingid'=>$epikmatching->id);
    $postcountsql="
SELECT
    COUNT(1)
FROM
    {forum_posts} fp
    INNER JOIN {forum_discussions} fd ON fp.discussion=fd.id
WHERE
    fp.userid=:userid AND fd.forum=:forumid";

    if ($forum->completiondiscussions) {
        $value = $forum->completiondiscussions <=
                 $DB->count_records('forum_discussions',array('forum'=>$forum->id,'userid'=>$userid));
        if ($type == COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }
    if ($forum->completionreplies) {
        $value = $forum->completionreplies <=
                 $DB->get_field_sql( $postcountsql.' AND fp.parent<>0',$postcountparams);
        if ($type==COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }
    if ($forum->completionposts) {
        $value = $forum->completionposts <= $DB->get_field_sql($postcountsql,$postcountparams);
        if ($type == COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }

    return $result;
}