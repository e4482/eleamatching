<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for epikmatching
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_epikmatching
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Jeu d\'appariement';
$string['modulenameplural'] = 'Appariements';
$string['modulename_help'] = ""
        . "<h2>Jeu d'appariement</h2>"
        . "<p>Cette activité permet de réaliser des associations par paires en permettant à l'élève de relier les éléments entre eux deux par deux.</p>";
$string['epikmatchingfieldset'] = 'Custom example fieldset';
$string['epikmatchingname'] = 'epikmatching';
$string['epikmatchingname_help'] = 'Exercice qui consiste à relier des items par paires à l\'aide de lignes';
$string['epikmatching'] = 'epikmatching';
$string['pluginadministration'] = 'Administration appariement';
$string['pluginname'] = 'epikmatching';
$string['grade'] = 'nombre de points';
$string['grade_help'] = 'Nombre de points attribués quand l\'activité est réussie.';
$string['epikmatching:addinstance'] = 'Ajouter une activité appariement';
$string['deletealluserdata'] = 'Supprimer toutes les tentatives des appariements';
$string['converttoquiz'] = 'Convertir en activité Test';
$string['correctfeedback'] = 'Bonne réponse !';
$string['partiallycorrectfeedback'] = 'Mauvaise réponse...';
$string['incorrectfeedback'] = 'Mauvaise réponse...';
$string['cantconvertcodeerror'] = 'Une erreur s\'est produite lors de la conversion du module, veuillez contacter l\'équipe de support';
