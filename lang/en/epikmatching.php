<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for epikmatching
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_epikmatching
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Matching';
$string['modulenameplural'] = 'Matching';
$string['modulename_help'] = ""
        . "#### Matching\n"
        . 'Association par paires.'
        . "";
$string['epikmatchingfieldset'] = 'Custom example fieldset';
$string['epikmatchingname'] = 'epikmatching';
$string['epikmatchingname_help'] = 'Make links between items to associate them';
$string['epikmatching'] = 'epikmatching';
$string['pluginadministration'] = 'epikmatching administration';
$string['pluginname'] = 'epikmatching';
$string['grade'] = 'number of points';
$string['grade_help'] = 'how many points are allocated when student succeeds.';
$string['epikmatching:addinstance'] = 'add module epikmatching';
$string['deletealluserdata'] = '';
$string['deletealluserdata'] = 'Delete all user data';
$string['converttoquiz'] = 'Convert to quiz';
$string['correctfeedback'] = 'Correct';
$string['partiallycorrectfeedback'] = 'Partially correct';
$string['incorrectfeedback'] = 'Incorrect';
$string['cantconvertcodeerror'] = 'Couldn\'t convert the module, please contact support';
