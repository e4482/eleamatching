<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * References files that should be automatically loaded
 *
 * @package    mod_epikmatching
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * A simple autoloader which makes it easy to load classes when you need them.
 *
 * @param string $class name
 */
function epikmatching_autoloader($class) {
    global $CFG;
    static $classmap;
    if (!isset($classmap)) {
        $classmap = array(
            'vikimodule\Editor' => 'vikimodule/Editor.php',
            'vikimodule\Activity' => 'vikimodule/Activity.php',
            'vikimodule\Question' => 'vikimodule/Question.php',
            'vikimodule\ElementWithTextEditor' => 'vikimodule/ElementWithTextEditor.php',
            'vikimodule\CommonQuiz' => 'vikimodule/CommonQuiz.php',
            'vikimodule\multichoice\MultichoiceAnswer' => 'vikimodule/multichoice/MultichoiceAnswer.php',
            'vikimodule\multichoice\MultichoiceQuestion' => 'vikimodule/multichoice/MultichoiceQuestion.php'
        );
    }

    if (isset($classmap[$class])) {
        require_once($CFG->dirroot . '/mod/epikmatching/' . $classmap[$class]);
    }
}
spl_autoload_register('epikmatching_autoloader');
