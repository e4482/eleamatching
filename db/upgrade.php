<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file keeps track of upgrades to the epikmatching module
 *
 * Sometimes, changes between versions involve alterations to database
 * structures and other major things that may break installations. The upgrade
 * function in this file will attempt to perform all the necessary actions to
 * upgrade your older installation to the current version. If there's something
 * it cannot do itself, it will tell you what you need to do.  The commands in
 * here will all be database-neutral, using the functions defined in DLL libraries.
 *
 * @package    mod_epikmatching
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_epikmatching\local\models\EpikmatchingActivity;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot . '/mod/epikmatching/autoloader.php');

/**
 * Execute epikmatching upgrade from the given old version
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_epikmatching_upgrade($oldversion) {
    global $DB, $CFG;

    $dbman = $DB->get_manager(); // loads ddl manager and xmldb classes

    // And upgrade begins here. For each one, you'll need one
    // block of code similar to the next one. Please, delete
    // this comment lines once this file start handling proper
    // upgrade code.

    // if ($oldversion < YYYYMMDD00) { //New version in version.php
    //
    // }

    // Lines below (this included)  MUST BE DELETED once you get the first version
    // of your module ready to be installed. They are here only
    // for demonstrative purposes and to show how the epikmatching
    // iself has been upgraded.

    // For each upgrade block, the file epikmatching/version.php
    // needs to be updated . Such change allows Moodle to know
    // that this file has to be processed.

    // To know more about how to write correct DB upgrade scripts it's
    // highly recommended to read information available at:
    //   http://docs.moodle.org/en/Development:XMLDB_Documentation
    // and to play with the XMLDB Editor (in the admin menu) and its
    // PHP generation posibilities.

    // First example, some fields were added to install.xml on 2007/04/01
    if ($oldversion < 2007040100) {

        // Define field course to be added to epikmatching
        $table = new xmldb_table('epikmatching');
        $field = new xmldb_field('course', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0', 'id');

        // Add field course
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field intro to be added to epikmatching
        $table = new xmldb_table('epikmatching');
        $field = new xmldb_field('intro', XMLDB_TYPE_TEXT, 'medium', null, null, null, null,'name');

        // Add field intro
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field introformat to be added to epikmatching
        $table = new xmldb_table('epikmatching');
        $field = new xmldb_field('introformat', XMLDB_TYPE_INTEGER, '4', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0',
            'intro');

        // Add field introformat
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Once we reach this point, we can store the new version and consider the module
        // upgraded to the version 2007040100 so the next time this block is skipped
        upgrade_mod_savepoint(true, 2007040100, 'epikmatching');
    }

    if ($oldversion < 2018061900) {

        // Define table epikmatching_success to be created.
        $table = new xmldb_table('epikmatching_success');

        // Adding fields to table epikmatching_success.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('epikmatching', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('attempts', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table epikmatching_success.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('epikmatching', XMLDB_KEY_FOREIGN, array('epikmatching'), 'epikmatching', array('id'));

        // Adding indexes to table epikmatching_success.
        $table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));

        // Conditionally launch create table for epikmatching_success.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Epikmatching savepoint reached.
        upgrade_mod_savepoint(true, 2018061900, 'epikmatching');
    }

    if ($oldversion < 2020111000) {
        // Adding a temporary table for processing problematic activities later
        $errortable = new xmldb_table('epikmatching_errors');

        if (!$dbman->table_exists($errortable)) {
            $field = new xmldb_field('id');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null);
            $errortable->addField($field);

            $field = new xmldb_field('epikmatching');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, 'id');
            $errortable->addField($field);

            $field = new xmldb_field('coursemodule');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, 'epikmatching');
            $errortable->addField($field);

            $field = new xmldb_field('questionids');
            $field->set_attributes(XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null , 'coursemodule');
            $errortable->addField($field);

            // primary key
            $key1 = new xmldb_key('primary');
            $key1->set_attributes(XMLDB_KEY_PRIMARY, array('id'), null, null);
            $errortable->addKey($key1);

            $dbman->create_table($errortable);
        }

        // To process conversion, get data before erasing it
        $modules = $DB->get_records('epikmatching', [], 'id', 'id');

        $table = new xmldb_table('epikmatching');

        // Define field questionids to be added to epikmatching.
        $field = new xmldb_field('questionids', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null, 'grade');

        // Conditionally launch add field questionids.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Upgrade of old modules
        $nbmodules = count($modules);
        $nextstep = $nbmodules / 10;
        $current = 0;
        mtrace("Upgrade of $nbmodules epik matching modules\n");
        $oldfilterall = $CFG->filterall;
        $CFG->filterall = false;
        foreach ($modules as $modulewithid) {
            $module = $DB->get_record('epikmatching', ['id' => $modulewithid->id], '*', MUST_EXIST);

            if (++$current > $nextstep) {
                $progress = (int) $current / $nbmodules * 100;
                mtrace("\t $progress%\n");
                $nextstep = $current + $nbmodules / 10;
            }
            if (empty($module->questionids)) {
                $epikmatching = EpikmatchingActivity::from_old_version($module);
                try {
                    $epikmatching->upgrade(true);
                } catch (dml_exception $exception) {
                    $error = new stdClass();
                    $error->epikmatching = $epikmatching->id;
                    $error->questionids = $epikmatching->questionids;
                    $epikmatchingmoduleid = $DB->get_record('modules', ['name' => 'epikmatching'], 'id', MUST_EXIST)->id;
                    $error->coursemodule = $DB->get_record(
                        'course_modules', ['module' => $epikmatchingmoduleid, 'instance' => $epikmatching->id],
                        'id', MUST_EXIST)->id;
                    $DB->insert_record('epikmatching_errors', $error);
                }
                $epikmatching->update_files();
            }
        }
        $CFG->filterall = $oldfilterall;
        mtrace("Upgrade completed\n");

        // Epikmatching savepoint reached.
        upgrade_mod_savepoint(true, 2020111000, 'epikmatching');
    }

    if ($oldversion < 2020121000) {

        $backuptable = new xmldb_table('epikmatching_backup');

        if (!$dbman->table_exists($backuptable)) {
            $field = new xmldb_field('id');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', true, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null);
            $backuptable->addField($field);

            $field = new xmldb_field('epikmatching');
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, 'id');
            $backuptable->addField($field);

            for ($iPair = 1 ; $iPair <= 9 ; $iPair++) {
                for ($iColonne = 1 ; $iColonne <= 2 ; $iColonne++) {
                    $field = new xmldb_field("pair{$iColonne}_{$iPair}");
                    $field->set_attributes(XMLDB_TYPE_TEXT, 'big', true, null, null, null, null);
                    $backuptable->addField($field);
                }
            }

            // primary key
            $key1 = new xmldb_key('primary');
            $key1->set_attributes(XMLDB_KEY_PRIMARY, array('id'), null, null);
            $backuptable->addKey($key1);

            $dbman->create_table($backuptable);
        }

        // Backup
        mtrace("Backuping epikmatching modules");
        foreach ($DB->get_records('epikmatching', [], '', 'id') as $modulewithid) {
            if ($DB->get_record('epikmatching_backup', ['epikmatching' => $modulewithid->id]) == false) {
                $module = $DB->get_record('epikmatching', ['id' => $modulewithid->id], '*', MUST_EXIST);
                $module->epikmatching = $module->id;
                unset($module->id);
                $DB->insert_record('epikmatching_backup', $module);
            }
        }
        mtrace("... complete\n");

        // Retrieving activities that may have been corrupted because of iframe usage
        if ($dbman->field_exists('epikmatching', 'pair1_1')) {
            $corruptedselectionarray = [];
            for ($iPair = 1 ; $iPair <= 9 ; $iPair++) {
                for ($iColonne = 1 ; $iColonne <= 2 ; $iColonne++) {
                    $corruptedselectionarray[] = "pair${iColonne}_$iPair like '%<iframe%'";
                }
            }

            $corrupted = $DB->get_records_select('epikmatching', implode(" OR ", $corruptedselectionarray));
            mtrace("Found " . count ($corrupted) . " possibly corrupted epikmatching modules\n");
            foreach ($corrupted as $module) {
                $newmodule = EpikmatchingActivity::from_old_version($module);
                try {
                    mtrace("Re-upgrading module $newmodule->id");
                    $newmodule->upgrade(true);
                    mtrace("... complete\n");
                } catch (dml_exception $exception) {
                    $error = new stdClass();
                    $error->epikmatching = $newmodule->id;
                    $error->questionids = $newmodule->questionids;
                    $epikmatchingmoduleid = $DB->get_record('modules', ['name' => 'epikmatching'], 'id', MUST_EXIST)->id;
                    $error->coursemodule = $DB->get_record(
                        'course_modules', ['module' => $epikmatchingmoduleid, 'instance' => $newmodule->id],
                        'id', MUST_EXIST)->id;
                    $DB->insert_record('epikmatching_errors', $error);
                    mtrace("... in error table.\n");
                }
                $newmodule->update_files();
            }
        }

        // Deleting useless fields
        $table = new xmldb_table('epikmatching');

        // Define field pairx_y to be dropped from epikmatching.
        // max hard coded as it was done before this update
        for ($ipair = 1 ; $ipair <= 9 ; $ipair++) {
            $field = new xmldb_field("pair1_${ipair}");

            // Conditionally launch drop field pair1_2.
            if ($dbman->field_exists($table, $field)) {
                $dbman->drop_field($table, $field);
            }

            $field = new xmldb_field("pair2_${ipair}");

            // Conditionally launch drop field pair1_2.
            if ($dbman->field_exists($table, $field)) {
                $dbman->drop_field($table, $field);
            }
        }

        if ($dbman->table_exists('epikmatching_errors')) {
            // Restablish questions for activities with errors
            $errorsdata = $DB->get_records('epikmatching_errors');
            $nbmodules = count($errorsdata);
            $nextstep = $nbmodules / 10;
            $current = 0;
            mtrace("Restoring $nbmodules Epikmatching modules\n");
            foreach ($errorsdata as $error) {
                if (++$current > $nextstep) {
                    $progress = (int) $current / $nbmodules * 100;
                    mtrace("\t $progress%\n");
                    $nextstep = $current + $nbmodules / 10;
                }
                $DB->set_field('epikmatching', 'questionids', $error->questionids, ['id' => $error->epikmatching]);
            }
            mtrace("Completed");
        }
    }

    if ($oldversion < 2020121603) {
        // Problematic questions
        $questions = $DB->get_records_sql('SELECT * FROM {question} WHERE questiontext LIKE "%draftfile%"');
        foreach ($questions as $question) {
            mtrace("Question $question->id");
            // get context id
            $contextid = $DB->get_record('question_categories', ['id' => $question->category], 'contextid', MUST_EXIST)->contextid;
            $filearea = 'questiontext';
            $itemid = $question->id;
            $question->questiontext = epikmatching_draft_to_plugin_file_fix($question->questiontext, $contextid, $filearea, $itemid);
            $DB->update_record('question', $question);
        }

        // Problematic answers
        $answers = $DB->get_records_sql('SELECT * FROM {question_answers} WHERE answer LIKE "%draftfile%"');
        foreach ($answers as $answer) {
            $questionid = $answer->question;
            mtrace("Answer $answer->id of question $questionid");
            $question = $DB->get_record('question', ['id' => $questionid], 'category', MUST_EXIST);
            $contextid = $DB->get_record('question_categories', ['id' => $question->category], 'contextid', MUST_EXIST)->contextid;
            $filearea = 'answer';
            $itemid = $answer->id;
            $answer->answer = epikmatching_draft_to_plugin_file_fix($answer->answer, $contextid, $filearea, $itemid);
            $DB->update_record('question_answers', $answer);
        }

        upgrade_mod_savepoint(true, 2020121603, 'epikmatching');
    }


    // And that's all. Please, examine and understand the 3 example blocks above. Also
    // it's interesting to look how other modules are using this script. Remember that
    // the basic idea is to have "blocks" of code (each one being executed only once,
    // when the module version (version.php) is updated.

    // Lines above (this included) MUST BE DELETED once you get the first version of
    // yout module working. Each time you need to modify something in the module (DB
    // related, you'll raise the version and add one upgrade block here.

    // Final return of upgrade result (true, all went good) to Moodle.
    return true;
}

function epikmatching_draft_to_plugin_file_fix(string $txt, $contextid, $filearea, $itemid): string {
    global $DB;

    $fileareas = [];
    for ($i = 1 ; $i <= 2 ; $i++) {
        for ($j = 1 ; $j <= 9 ; $j++) {
            $fileareas[] = "pair${i}_${j}";
        }
    }
    $itemids = [0, 1];
    $fs = get_file_storage();

    // Start searching for files
    while (preg_match('#(^.*<img src=")([^ ]*draftfile[^ ]*)(" .*$)#m', $txt, $matches)) {
        $endoflink = mb_strrchr($matches[2], '/');
        $filename = substr($endoflink, 1);
        if (strpos($endoflink, '?') !== false) {
            $filename = mb_strstr($filename, '?', true);
        }
        $filename = urldecode($filename);
        mtrace("\tFile $filename ");

        // Fix file
        if ($fs->file_exists($contextid, 'question', $filearea, $itemid, '/', $filename)) {
            mtrace("... already exists");
        } else {
            $newdata = [
                'contextid' => $contextid,
                'component' => 'question',
                'filearea' => $filearea,
                'itemid' => $itemid];
            $found = false;
            foreach ($fileareas as $fileareaold) {
                foreach ($itemids as $itemidold) {
                    if ($fs->file_exists($contextid, 'mod_epikmatching', $fileareaold, $itemidold, '/', $filename)) {
                        $found = true;
                        $file = $fs->get_file($contextid, 'mod_epikmatching', $fileareaold, $itemidold, '/', $filename);
                        $fs->create_file_from_storedfile($newdata, $file);
                        mtrace("Copying file in question component\n");
                        // Fix text
                        $txt = $matches[1] . "@@PLUGINFILE@@" . $endoflink . $matches[3];
                        break 2;
                    }
                }
            }
        }
        if (!$found) {
            mtrace("not found at all.");
            // Do not try again
            break;
        }
    }
    mtrace("\tnew version of the text : $txt");
    return $txt;
}

