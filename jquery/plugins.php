<?php

$plugins = array(
    'jsplumb-jquerymodule' => array(
        'files' => array(
            'jquerymodule/jquery-jsplumb-1.7.4.js',
        ),
     ),
    'jqueryuitouch-jquerymodule' => array(
        'files' => array(
            'jquerymodule/jqueryui-touch-0.2.3.js',
        ),
     )    
);
