<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Internal library of functions for module epikmatching
 *
 * All the epikmatching specific functions, needed to implement the module
 * logic, should go here. Never include this file from your lib.php!
 *
 * @package    mod_epikmatching
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use vikimodule\Editor;
use mod_epikmatching\local\models\EpikmatchingActivity;

defined('MOODLE_INTERNAL') || die();


class epikmatchingNavigationModule {

  protected $courseid;
  protected $current_module_type;
  protected $current_module_id;
  protected $current_course_module;
  protected $section_parent;

  /***************************************************
   *
   * return @array
   *
   * *************************************************/
  public function __construct($courseid, $moduletype, $moduleid) {

    $this->courseid = $courseid;
    $this->current_module_type = $moduletype;
    $this->current_module_id = $moduleid;

  }
  /***************************************************
   *
   * return @array
   *
   * *************************************************/
  public function get_modules_list() {
    global $DB;
    $course_sections = $DB->get_records_sql("
        SELECT id, sequence
        FROM {course_sections}
        WHERE course = '" . $this->courseid . "'
        ORDER BY section ASC");
    $global_sequence = "";
    $final_array = [];
    foreach($course_sections as $course_section) {
        $global_sequence .= $course_section->sequence . ",";
    }

    $course = $DB->get_record('course',array('id' => $this->courseid));
    $modinfo = get_fast_modinfo($course);
    $modules_array = explode(",", $global_sequence);
    foreach ($modules_array as $cmid) {
      if ($cmid) {
        $cm = $modinfo->get_cm($cmid);
        if (($cm->visible) && ($cm->uservisible)) {
          array_push($final_array, $cmid);
        }
      }
    }
    // get the next module
    $this->current_course_module = $DB->get_record_sql("
        SELECT cm.id
        FROM {course_modules} as cm, {modules} as m
        WHERE cm.course = '" . $this->courseid . "'
        AND cm.module = m.id
        AND m.name = '" . $this->current_module_type . "'
        AND cm.instance = " . $this->current_module_id . "
    ");

    return $final_array;

  }
  public function get_parent_section($course_module_id) {

    global $DB;
    $parent_section = null;
    $course_sections = $DB->get_records_sql("
      SELECT id, sequence, section
      FROM {course_sections}
      WHERE course = '" . $this->courseid . "'
      ORDER BY section ASC");
    $global_sequence = "";
    foreach($course_sections as $course_section) {
      $sequence_array = explode(",", $course_section->sequence);
      if (in_array($course_module_id, $sequence_array)) {
        $parent_section = $course_section->section;
      }
    }
    return $parent_section;

  }
  /****************************************************
   *
   *
   *
   * **************************************************/

  public function html() {

    global $DB, $CFG;

    $global_sequence_array = $this->get_modules_list();
    $nextModule = "";
    $key = array_search($this->current_module_id, $global_sequence_array);

    if ($key !== false) {
      if (array_key_exists($key + 1, $global_sequence_array) &&
        $global_sequence_array[$key + 1] != '') {
        $next_course_module = $DB->get_record_sql("
          SELECT cm.instance, m.name
          FROM {course_modules} as cm, {modules} as m
          WHERE cm.id = " . $global_sequence_array[$key + 1] . "
          AND m.id = cm.module
        ");
        if ($next_course_module->name != 'label') {
          $nextModule = "<a href='/mod/"
            . $next_course_module->name
            . "/view.php?id="
            . $global_sequence_array[$key + 1]
            . "'><img src='/mod/epikmatching/pix/next.png' title='étape suivante' alt='next' style='margin:5px;' /></a>";
        }
        else {
          $nextModule = "<a href='/course/"
            . "view.php?id="
            . $this->courseid
            . "&section="
            . $this->get_parent_section($global_sequence_array[$key + 1])
            . "'><img src='/mod/epikmatching/pix/next.png' title='étape suivante' style='margin:5px;' alt='next' /></a>";
        }
      }

    }
    return $nextModule;
  }
}




function epikmatching_extract_themes() {
    return array('connecteurs');
}

function epikmatching_shuffle_assoc(&$array) {
    $keys = array_keys($array);

    shuffle($keys);
    $new = Array();

    foreach($keys as $key) {
        $new[$key] = $array[$key];
    }

    $array = $new;

    return true;
};

function epikmatching_create_game(EpikmatchingActivity $epikmatching, int $map){
    global $USER, $DB, $CFG, $COURSE;

    $wwwroot = $CFG->wwwroot;
    $cm = get_coursemodule_from_instance('epikmatching', $epikmatching->id, $epikmatching->course, false, MUST_EXIST);
    $contextmodule = context_module::instance($cm->id);

    $module_type = 'epikmatching';
    $module_id = $cm->id;
    $navigationModuleButton = new epikmatchingNavigationModule($COURSE->id, $module_type, $module_id);
    $nextModule = $navigationModuleButton->html();

    $gameid = "game" . rand(100,999);
    $gameid_reload = $gameid . "_reload";
    $gameid_won = $gameid . "_won";
    $gameid_gamecontainer = $gameid . "_gamecontainer";
    $title = $epikmatching->name;
    $expectedSolution = Array();
    $providedSolution = Array();
    $rightitems = Array();
    $leftitems = Array();

    $desc = $epikmatching->intro_editor($contextmodule->id)->get_filtered_content($contextmodule, 'mod_epikmatching');
    $pairs = $epikmatching->getPairs();
    for ($i = 0; $i < count($pairs) ; $i++) {
        $pair = $pairs[$i];
        $expectedSolution['left' . $i] = 'right' . $i;
        $providedSolution['left' . $i] = "";
        $rightitems['right' . $i] = $pair->right->get_filtered_content($contextmodule, 'mod_epikmatching');
        $leftitems['left' . $i] = $pair->left->get_filtered_content($contextmodule, 'mod_epikmatching');
    }
    // backward compatibility for php 5.3
    if (!defined(JSON_PRETTY_PRINT)) {
        define(JSON_PRETTY_PRINT, 128);
    }
    $expectedSolution_json = json_encode($expectedSolution, JSON_PRETTY_PRINT);
    $providedSolution_json = json_encode($providedSolution, JSON_PRETTY_PRINT);

    epikmatching_shuffle_assoc($rightitems);
    epikmatching_shuffle_assoc($leftitems);


    $firstlist = "";
    foreach ($leftitems as $key => $item) {
        $firstlist .="<li id='" . $key . "' class='pays'>" . $item . "</li>";
    }

    $secondlist = "";
    foreach ($rightitems as $key => $item) {
        $secondlist .="<li id='" . $key . "' class='capitale'>" . $item . "</li>";
    }

    $mathjax = "";

    $maplogo = "";
    if ($map) {
        $maplogo = "<a href='$wwwroot/mod/mapmodules/view.php?id=$map'><img src='$wwwroot/mod/epikmatching/pix/map.png' alt='Retourner à la carte' title='Retourner à la carte'></a>";
    }
    else {
        // get moodle section id and name
        //$section = $DB->get_record('course_sections', array('id' => $cm->section));
        //$courseid = $cm->course;
        //$sectionnumber = $section->section;
        //$maplogo = "<a href='$wwwroot/course/view.php?id=$courseid&section=$sectionnumber'><img src='$wwwroot/mod/epikmatching/pix/map.png' alt='Retourner à la section parente' title='Retourner à la section parente'></a>";
        $maplogo = "";
    }

    $game = <<<EOT
    <link rel="stylesheet" type="text/css" href="$wwwroot/mod/epikmatching/css/style.css"/>
    <div class="gamecontainer" id="$gameid_gamecontainer">
        <div class="won" id="$gameid_won">

        </div>
        <header class="epiktitle">
            <h2>$title</h2>
            <h4>$desc</h4>
            <div class="corner"></div>
        </header>
        <div id="$gameid" class="game">
            <div class="left">
                <ul>
                    $firstlist
                </ul>
            </div>
            <div class="right">
                <ul>
                    $secondlist
                </ul>
            </div>
        </div>
        <nav class="verify">
            <button class="epikbtn" id="validate_$gameid">Vérifier la réponse</button>
        </nav>



    </div>
    <script>
    YUI().use("node-base", function(Y){
        Y.on("domready", function(){
            var hasClass = function(ele,cls) {
                return (' ' + ele.className + ' ').indexOf(' ' + cls + ' ') > -1;
            }

            var addClass = function(ele,cls) {
              if (!hasClass(ele,cls)) ele.className += " "+cls;
            }

            var removeClass = function(ele,cls) {
              if (hasClass(ele,cls)) {
                var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
                ele.className=ele.className.replace(cls,' ');
              }
            }
            var expectedSolution = $expectedSolution_json;
            var providedSolution = $providedSolution_json;
            var example3Color = "rgba(213,92,75,1)";
            var example4Color = "rgba(223,115,27,0.5)";
            var exampleEndpoint3 = {
                endpoint:["Dot", {radius:14, hoverClass:"test", cssClass:"endpoint"} ],
                anchor:"BottomRight",
                paintStyle:{ fillStyle:example3Color, opacity:1},
                hoverPaintStyle:{ fillStyle:example4Color, opacity:0.5 },
                isSource:true,
                scope:'yellow',
                connector: ["Bezier", { curviness:100 , hoverClass:"test2"} ],
                connectorStyle:{ strokeStyle:example3Color, lineWidth:4 },
                connectorHoverStyle: { strokeStyle:example4Color, opacity:0.5 },
                maxConnections:1,
                isTarget:true
            };
            /*
             * init game
             */
            var createDots = function() {
              console.log("yop")
              jsPlumb.ready(function() {
                  var container = document.getElementById("$gameid");
                  jsPlumb.setContainer(container);

                  var capitales = document.getElementsByClassName("capitale");
                  for (var i = 0; i< capitales.length;i++) {
                      jsPlumb.addEndpoint(capitales[i], { anchor:"LeftMiddle" }, exampleEndpoint3);
                  }

                  var pays = document.getElementsByClassName("pays");
                  for (var i = 0; i< pays.length;i++) {
                      jsPlumb.addEndpoint(pays[i], { anchor:"RightMiddle" }, exampleEndpoint3);
                  }

              });
              /*
               * create connector
               */
              jsPlumb.bind("connection", function(info) {
                  var source = "";
                  var target = "";
                  if (info.source.parentNode.parentNode.getAttribute("class") == "right") {
                      target = info.source;
                      source = info.target;
                  }
                  else {
                      target = info.target;
                      source = info.source;
                  }
                  var targetId = target.getAttribute("id");
                  var sourceId = source.getAttribute("id");
                  providedSolution[sourceId] = targetId;
                  console.log(providedSolution);

              });
              /*
               * remove connector if source and dest are in the same column
               */
              jsPlumb.bind("beforeDrop", function(info) {
                  var source = document.getElementById(info.sourceId);
                  var target = document.getElementById(info.targetId);
                  if (source.parentNode.parentNode.getAttribute("class") == target.parentNode.parentNode.getAttribute("class")) {
                   return false;
                  }
                  return true;
              });
              /*
               * remove connector
               */
              jsPlumb.bind("connectionDetached", function(info) {
                  var source = "";
                  if (info.source.parentNode.parentNode.getAttribute("class") == "right") {
                      source = info.target;
                  }
                  else {
                      source = info.source;
                  }
                  var sourceId = source.getAttribute("id");
                  providedSolution[sourceId] = "";


                  console.log("connection detached");
              });
              var attempts = 1;
              var validate = document.getElementById("validate_$gameid");
              validate.addEventListener("click", function(ev){
                  ev.preventDefault();
                  var failure = 0;

                  for (var key in expectedSolution) {
                      if (expectedSolution[key] != providedSolution[key]) {
                          failure++;
                          if (providedSolution[key]) {
                              var el = document.getElementById(providedSolution[key]);
                              removeClass(el, "success");
                              removeClass(el, "fail");
                              addClass(el, "fail");
                          }
                          var el = document.getElementById(key);
                          removeClass(el, "success");
                          removeClass(el, "fail");
                          addClass(el, "fail");
                      }
                      else {
                          if (providedSolution[key]) {
                          var el = document.getElementById(providedSolution[key]);
                              removeClass(el, "success");
                              removeClass(el, "fail");
                              addClass(el, "success");
                          }
                          var el = document.getElementById(key);
                          removeClass(el, "success");
                          removeClass(el, "fail");
                          addClass(el, "success");
                      }
                  }
                  if (failure == 0) {

                      var xhReq = new XMLHttpRequest();
                      xhReq.open("GET", "$wwwroot/mod/epikmatching/validate.ajax.php?mid=$epikmatching->id&attempts="+attempts, false);
                      xhReq.send(null);

                      xhReq.open("GET", "/mod/epikmatching/getnextmodule.ajax.php?epikmatchingid=$epikmatching->id&sesskey=$USER->sesskey", false);
                      xhReq.onreadystatechange = function () {
                        if(xhReq.readyState === XMLHttpRequest.DONE && xhReq.status === 200) {
                            var response = JSON.parse(xhReq.response)
                            if (response.contents == "") {
                              var subtitle = "Vous pouvez retourner dans votre espace à présent"
                              var nextModule = "<a href='$wwwroot/my'><img src='/mod/epikmatching/pix/home.png' alt='retour à mon espace' title='retour à mon espace'></a>"
                            }
                            else {
                              var subtitle = "Passons à l'étape suivante"
                              var nextModule = response.contents
                            }
                            var won = document.getElementById("$gameid_won");
                            var winheight = $(window).height();
                            var winwidth = $(window).width();
                            var scrolltop = $(window).scrollTop();
                            var containerheight = document.getElementById("$gameid_gamecontainer").offsetHeight;

                            var newdiv = document.createElement("div");
                            newdiv.setAttribute("style", "position: absolute;width:100%;left:0px;top:0px;height:100%;background-color: black; z-index:2000;opacity: 0.6;height:100%;");
                            document.body.appendChild(newdiv);

                            var newdiv = document.createElement("div");
                            newdiv.innerHTML = "<div style='display:flex;flex-direction: column;width:40%;background-color: #47A3DA;'>"
                              + "<div style='text-align: center;background-color:#F5EFEF;'>"
                              + "<img style='height:150px;margin-top:20px;' src='$wwwroot/mod/epikmatching/pix/congratz.png' alt='félicitations'>"
                              + "<h2 style='color:black;'>Bravo !</h2>"
                              + "<p style='color:black;'>" + subtitle + "</p>"
                              + "</div>"
                              + "<div style='height:30px;text-align:center;'><img style='vertical-align: inherit;width:40px;' src='$wwwroot/mod/epikmatching/pix/arrow.png' alt='suivant'></div>"
                              + "<div style='text-align: center;'>"
                              + "<p>$maplogo " + nextModule + "</p>"
                              + "</div>"
                              + "</div>"

                            document.body.appendChild(newdiv);
                            newdiv.setAttribute("style", "left:0px;position:absolute;display:flex;align-items: center;justify-content: center;z-index:2001;height:100%;width:100%;top:0px;");
                        }
                      };
                      xhReq.send(null);

                  }
                  else {
                      console.log("perdu "+attempts);
                      attempts++;
                  }
              });
            }
            if (typeof MathJax !== 'undefined') {
                MathJax.Hub.Queue(function () {
                  createDots()
                })
            }
            else {
              createDots()
            }
        });
    });
    </script>
EOT;
    return $game;
}
