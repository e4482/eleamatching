<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of epikmatching
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_epikmatching
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/// (Replace epikmatching with the name of your module and remove this line)

use mod_epikmatching\local\models\EpikmatchingActivity;

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');

$id = optional_param('id', 0, PARAM_INT); // course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // epikmatching instance ID - it should be named as the first character of the module

$map = optional_param('map', 0, PARAM_INT);

if ($id) {
    $cm         = get_coursemodule_from_id('epikmatching', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $epikmatching  = new EpikmatchingActivity((int)($cm->instance));
} elseif ($n) {
    $epikmatching  = new EpikmatchingActivity((int)$n);
    $course     = $DB->get_record('course', array('id' => $epikmatching->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('epikmatching', $epikmatching->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);
$context = context_module::instance($cm->id);

/// Print the page header

$PAGE->set_url('/mod/epikmatching/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($epikmatching->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);
$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('jqueryuitouch-jquerymodule', 'mod_epikmatching');
$PAGE->requires->jquery_plugin('jsplumb-jquerymodule', 'mod_epikmatching');
//$PAGE->requires->js(new moodle_url("https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML"));
// Output starts here
echo $OUTPUT->header();
echo epikmatching_create_game($epikmatching, $map);
echo $OUTPUT->footer();
