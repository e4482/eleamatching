<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Ajax script to update the contents of the question bank dialogue.
 *
 * @package    mod_epikmatching
 * @copyright  2016 Pascal Fautrero
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);
global $DB, $USER;

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/epikmatching/locallib.php');
require_once($CFG->dirroot . '/mod/epikmatching/lib.php');

$epikmatchingid = required_param('epikmatchingid', PARAM_INT);
$epikmatching   = $DB->get_record('epikmatching', array('id' => $epikmatchingid), '*', MUST_EXIST);
$course         = $DB->get_record('course', array('id' => $epikmatching->course), '*', MUST_EXIST);
$cm             = get_coursemodule_from_instance('epikmatching', $epikmatching->id, $course->id, false, MUST_EXIST);
$context        = context_module::instance($cm->id);
require_login($course, false, $cm);
if (is_guest($context, $USER)) {
    throw new moodle_exception('noguestsubscribe', 'mod_epikmatching');
}

$module_type = 'epikmatching';
$module_id = $cm->id;
rebuild_course_cache($course->id, true);
$navigationModuleButton = new epikmatchingNavigationModule($course->id, $module_type, $module_id);
$nextModule = $navigationModuleButton->html();

echo json_encode(array(
    'status'   => 'OK',
    'contents' => $nextModule,
));
