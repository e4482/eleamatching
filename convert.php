<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/locallib.php');
require_once(dirname(__FILE__).'/lib.php');
global $CFG;
require_once($CFG->dirroot.'/course/modlib.php');
require_once($CFG->dirroot.'/mod/quiz/lib.php');

use mod_epikmatching\local\models\EpikmatchingActivity;
use vikimodule\CommonQuiz;

$cmid = required_param('cmid', PARAM_INT);
$courseid = required_param('courseid', PARAM_INT);

require_login($courseid);

$returnurl = new moodle_url('/course/view.php', ['id' => $courseid]);

if (!has_capability('mod/quiz:addinstance', context_course::instance($courseid))) {
    redirect($returnurl, get_string('cantconvertcapabilityerror', MOD_EPIKMATCHING_MODNAME));
}

try {
    global $DB;

    // Retrieve data from DB
    $course = $DB->get_record('course', ['id' => $courseid], '*', MUST_EXIST);

    // Create quiz with old module
    $standardquiz = new CommonQuiz($cmid, [
        'questionsperpage' => 0,
        'preferredbehaviour' => 'deferredfeedback',
        'specificfeedbackduring' => null,
        'attemptimmediately' => 1,
        'correctnessimmediately' => 1]);
    if ($course->format == 'singleactivity') {
        $standardquiz->create_single_activity_course($course);
    } else {
        $standardquiz->switch_to_quiz_in_course($course);
    }
    $returnurl = new moodle_url('/course/view.php', ['id' => $standardquiz->get_course_id()]);

    // Retrieving questions
    $epikmatching = new EpikmatchingActivity($standardquiz->get_old_cm_instance());
    $standardquiz->add_questions($epikmatching->get_questionids(), 0);

    redirect($returnurl);

} catch (Exception $e) {
    error_log($e->getMessage());
    error_log($e->getTraceAsString());
    redirect($returnurl, get_string('cantconvertcodeerror', MOD_EPIKMATCHING_MODNAME));
}
