<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_epikmatching
 * @subpackage backup-moodle2
 * @copyright 2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_epikmatching_activity_task
 */

use mod_epikmatching\local\models\EpikmatchingActivity;
use vikimodule\Question;

/**
 * Structure step to restore one epikmatching activity
 */
class restore_epikmatching_activity_structure_step extends restore_questions_activity_structure_step {

    /**
     * @var EpikmatchingActivity
     */
    private $newactivity;

    protected function define_structure() {

        $paths = array();
        //$userinfo = $this->get_setting_value('userinfo');
        $paths[] = new restore_path_element('epikmatching', '/activity/epikmatching');
       
        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_epikmatching($epikmatching) {
        global $DB;

        $epikmatching = (object)$epikmatching;
        if (empty($epikmatching->questionids)) {
            // Old version case: upgrade (files not updated right now but after execute
            $this->newactivity = EpikmatchingActivity::from_old_version($epikmatching, $this->task->get_moduleid());
            $this->newactivity->upgrade(false);
            $epikmatching->questionids = $this->newactivity->questionids;
        } else {
            // Newer version case : update question ids
            $oldquestionids = explode(Question::QUESTION_DELIMITER, $epikmatching->questionids);
            $newquestionids = [];
            foreach ($oldquestionids as $oldquestionid) {
                $newquestionids[] = $this->get_mappingid('question', $oldquestionid);
            }
            $epikmatching->questionids = implode(Question::QUESTION_DELIMITER, $newquestionids);
            // To be deleted when fields are dropped
            for ($i = 1; $i <= 9; $i++) {
                for ($j = 1; $j <= 2; $j++ ) {
                    $fieldname = "pair{$j}_{$i}";
                    $epikmatching->$fieldname = '';
                }
            }
        }

        $epikmatching->course = $this->get_courseid();

        // insert the epikmatching record
        $newitemid = $DB->insert_record('epikmatching', $epikmatching);
        $this->apply_activity_instance($newitemid);
    }

    protected function after_execute() {
        global $DB;

        parent::after_execute();

        // Add epikmatching related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_epikmatching', 'intro', null);
        // For compatibility
		for ($i=1;$i<=9;$i++) {
			$pair1 = 'pair1_' . $i;
			$pair2 = 'pair2_' . $i;
			$this->add_related_files('mod_epikmatching', $pair1, null);
			$this->add_related_files('mod_epikmatching', $pair2, null);
		}
        // Upgrade if from an old module
        if  ($this->newactivity) {
            $this->newactivity->update_files();
        }
    }

    /**
     * When process_question_usage creates the new usage, it calls this method
     * to let the activity link to the new usage. For example, the quiz uses
     * this method to set quiz_attempts.uniqueid to the new usage id.
     * @param integer $newusageid
     */
    protected function inform_new_usage_id($newusageid)
    {
        // TODO: Implement inform_new_usage_id() method.
    }
}
